<?php
/**
 * @file
 * Set up for the news migration.
 */
function typo3_news_migrate_install() {
  typo3_news_migrate_content_type();
  typo3_news_migrate_category();
  
  typo3_news_migrate_fields();
}

function typo3_news_migrate_uninstall() {
  if ($vocabs = taxonomy_get_vocabularies('news')) {
    // Grab key of the first returned vocabulary.
    taxonomy_del_vocabulary(key($vocabs));
  }
  typo3_news_migrate_content_type_delete();
}

/**
 * 
 * Create content type "news".
 */
function typo3_news_migrate_content_type() {
  $types = array(
    array(
      'type' => 'news',
      'name' => st('News'),
      'module' => 'node',
      'description' => st("News from typo3 tt_news."),
      'custom' => 1,
      'modified' => 1,
      'locked' => 1,
    ),
  );

  foreach ($types as $type) {
    $type = node_type_set_defaults($type);
    node_type_save($type);
  }
}

/**
 * 
 * Create fields for the news content type.
 */
function typo3_news_migrate_fields() {
  // field for news images
  $field = array(
    'field_name' => 'field_news_images',
    'type' => 'filefield',
    'multiple' => '1',
    'cardinality' => 5,
    'widget_type' => 'imagefield_widget',
    'type_name' => 'news',
    'label' => t('News Image'),
  );
  content_field_instance_create($field);

  // field for news files
  $field = array(
    'field_name' => 'field_news_files',
    'type' => 'filefield',
    'multiple' => '1',
    'cardinality' => 5,
    'widget_type' => 'filefield_widget',
    'type_name' => 'news',
    'label' => t('News File'),
  );
  content_field_instance_create($field);

  // field for news sub-headers
  $field = array(
    'field_name' => 'field_news_sub_header',
    'type' => 'text',
    'widget_type' => 'text_textfield',
    'type_name' => 'news',
    'label' => t('News Sub Header'),
  );
  content_field_instance_create($field);

  // field for news author
  $field = array(
    'field_name' => 'field_news_author',
    'type' => 'text',
    'size' => '60',
    'widget_type' => 'text_textfield',
    'type_name' => 'news',
    'label' => t('News Author'),
  );
  content_field_instance_create($field);

  // field for news author email
  $field = array(
    'field_name' => 'field_news_author_email',
    'type' => 'text',
    'size' => '60',
    'widget_type' => 'text_textfield',
    'type_name' => 'news',
    'label' => t('Author Email'),
  );
  content_field_instance_create($field);
  
  // nodereference field for related articles.
  $field = array(
    'field_name' => 'field_news_related_articles',
    'type' => 'nodereference',
    'widget_type' => 'nodereference_select',
    'type_name' => 'news',
    'multiple' => '1',
    'referenceable_types' => 
      array(
        'news' => 'news'
      ),
    'module' => 'nodereference',
    'widget_module' => 'nodereference',    
    'label' => t('Related Articles'),
  );
  content_field_instance_create($field);
}

function typo3_news_migrate_content_type_delete() {
  $bundle = 'news';
  node_type_delete($bundle);
}

/**
 * 
 * Create "News Category" vocabulary.
 */
function typo3_news_migrate_category() {
  // Create a vocabulary named "News Category", enabled for the 'news' content type.
  $description = st('News categories.');
  $help = st('Enter a comma-separated list of tags to describe your content.');
  $vocabulary = array(
    'name' => 'News Category',
    'description' => $description,
    'machine_name' => 'news_category',
    'help' => $help,
    'tags' => 1,
    'multiple' => 1,
    'required' => 0,
    'hierarchy' => 1,
    'relations' => 0,
    'module' => 'typo3_migrate',
    'nodes' => array('news' => 1),
  );

  taxonomy_vocabulary_save($vocabulary);
}
